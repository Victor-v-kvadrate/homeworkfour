// Задание 1.1
for (let a = 1; a <= 100; a++){
    if (a % 3 === 0 && a % 5 == 0){
        console.log('FizzBuzz')
    } else if( a % 3 === 0){
        console.log('Fizz')
    } else if( a % 5 === 0){
        console.log('Buzz')
    } else{
        console.log(a)
    }
}

// Задание 1.1.1
let str = 'Лето удалось очень жарким, было много ягод и фруктов, лучшим местом для отдыха была речка.';
console.log(str);
let strNew = str.replace(/,/g, '.');
console.log(strNew);

// Задание 1.1.2
let strArr = str.split(', ')
console.log(strArr)
let strUp = strArr.map(function(item,i){
    return item[0].toUpperCase(0) + item.slice(1);
});
strDote = strUp.join(", ");
console.log(strDote);

// Задание 2
let strName = "Антон, Дима и Женя пришли на День рождения сразу, а Миша пришел позже";
let strNameTwo = strName.split(' ')
strNameOne = strNameTwo.filter(function(names){
    return names === "Миша"
})
let strFinish = `На день рождения пришли: ${strName.slice(0,11) + ', ' + strName.slice(14,18) + ", " + strNameOne.join()}`;
console.log(strFinish)

